import os
import requests
import random
import json
from pprint import pprint
from time import sleep





delay = 10
iterations = 90
#serviceUrl = 'https://24a390ee-67e8-436d-8a95-b984f9cab0c1.mock.pstmn.io/'
serviceUrl = 'http://host.docker.internal:8080/'
finalRuntimeGatewayUrl = ''

def createUrl(path):
    return serviceUrl + path;

def waitForCompletion(deploymentID):
    url = createUrl('v1/deployments/' + deploymentID + '/status');
    pprint(url)
    headers = {
        'Org-Isolation-ID': 'f067aac4-23f1-48e1-9258-1543b3d0b528',
        'Content-Type': 'application/json'
        }
    for x in range(iterations):
        response = requests.request("GET", url, headers=headers, data={})
        pprint(response.json())
        if (response.json() != 'INPROGRESS'):
            break;
        sleep(5)
    response =  requests.request("GET", url, headers=headers, data={})
    if (response.text == 'FAILED'):
        raise Exception('Application deployment failed!!!')
    pprint('Application deployment completed.')


def deployApplication():
    # Write 'Hello, World' to standard output.
    pprint(os.environ["VERSION"])
    # pprint($RUNTIME_GATEWAY_URL)
    pprint('Executing deployment job for GoMechanic application')
    url = createUrl('v1/deployments')
    payload = json.dumps({
            "buildID":"27f3cb76-5ddb-4f4e-8265-3498fb3cf2f0"
        })
    headers = {
        'Org-Isolation-ID': 'f067aac4-23f1-48e1-9258-1543b3d0b528',
        'Content-Type': 'application/json'
        }   

    response = requests.request("POST", url, headers=headers, data=payload)
    #url = 'http://host.docker.internal:8080'
    pprint(url)
    responsJson = response.json();
    pprint(responsJson)
    
    waitForCompletion(responsJson['deploymentID'])
    
    
    url1 = createUrl('v1/deployments/' + responsJson['deploymentID'])
    response1 = requests.request("GET", url1, headers=headers, data={})
    responsJson1 = response1.json();
    pprint(responsJson1)
    os.environ["RUNTIME_GATEWAY_URL"] = str(responsJson1['runtimeGatewayEndpoint'])
    pprint(os.environ["RUNTIME_GATEWAY_URL"])
    # test_basic_headless_selenium_example()
    os.environ["DEPLOYMENT_ID"] = str(responsJson1['deploymentID'])
    pprint(os.environ["DEPLOYMENT_ID"])
    #cleanup(responsJson['deploymentID'])

# def test_basic_headless_selenium_example():
#     """Test selenium installation by opening python website.
#     (inspired by https://selenium-python.readthedocs.io/getting-started.html)
#     """
#     opts = Options()
#     opts.headless = True
#     driver = webdriver.Firefox(options=opts)
#     pprint(finalRuntimeGatewayUrl)
#     driver.get(finalRuntimeGatewayUrl)
#     driver.close()
    
def cleanup(deploymentID):
    # Write 'Hello, World' to standard output.
    pprint('Executing deployment cleanup for GoMechanic application')
    url = createUrl('v1/deployments/'+ deploymentID)
    payload={}
    headers = {
        'Org-Isolation-ID': 'f067aac4-23f1-48e1-9258-1543b3d0b528',
        'Content-Type': 'application/json'
        }   

    response = requests.request("DELETE", url, headers=headers, data=payload)
    #url = 'http://host.docker.internal:8080'

deployApplication()

