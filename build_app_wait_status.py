import os
import requests
import random
import json
from pprint import pprint
from time import sleep

delay = 10
iterations = 90
#serviceUrl = 'https://4b68c1bf-b7b9-46f8-ac2f-0fc516248200.mock.pstmn.io/'
serviceUrl = 'http://host.docker.internal:8080/'

def createUrl(path):
    return serviceUrl + path;

def waitForCompletion(buildID):
    url = createUrl('v1/builds/' + buildID + '/status');
    print(url)
    for x in range(iterations):
        response = requests.get(url)
        if (response.text != 'SCHEDULED'):
            break;
        sleep(5)
    response = requests.get(url)
    if (response.text == 'FAILED'):
        raise Exception('Application build failed!!!')
    print('Application build completed.')


def buildApplication():
    # Write 'Hello, World' to standard output.
    # print(f'{RUNTIME_GATEWAY_URL}')
    # echo "RUNTIME_GATEWAY_URL='world'"
    # print(f'{RUNTIME_GATEWAY_URL}')
    # refName = os.environ.get("VERSION")
    # pprint(refName)
    # os.environ["VERSION"] = 'aabhas local'
    # pprint(os.environ["VERSION"])
    
    print('Executing build job for GoMechanic application')
    url = createUrl('v1/builds')
    #url = 'http://host.docker.internal:8080'
    print(url)
    payload = json.dumps({
        "applicationID": "3a360988-abae-484d-b2c8-12b0d55601f3",
        "branch": "main",
        "isolationID": "a21a1909-c664-41be-bfc5-9a3ce0dae52c"
    })
    headers = {
    'Org-Isolation-ID': 'a21a1909-c664-41be-bfc5-9a3ce0dae52c',
    'Content-Type': 'application/json'
    }   

    response = requests.request("POST", url, headers=headers, data=payload)
    responsJson = response.json();
    pprint(responsJson)
    waitForCompletion(responsJson['buildID'])

buildApplication()
