import os
import requests
import json
from pprint import pprint

delay = 10
iterations = 90
#serviceUrl = 'https://24a390ee-67e8-436d-8a95-b984f9cab0c1.mock.pstmn.io/'
serviceUrl = 'http://host.docker.internal:8080/'
finalRuntimeGatewayUrl = ''

def createUrl(path):
    return serviceUrl + path;

def cleanup():
    # Write 'Hello, World' to standard output.
    deploymentID = os.environ["DEPLOYMENT_ID"]
    pprint('deployment id:')
    pprint(deploymentID)
    pprint('Executing deployment cleanup for GoMechanic application')
    url = createUrl('v1/deployments/'+ deploymentID)
    payload={}
    headers = {
        'Org-Isolation-ID': 'f067aac4-23f1-48e1-9258-1543b3d0b528',
        'Content-Type': 'application/json'
        }   

    response = requests.request("DELETE", url, headers=headers, data=payload)

cleanup()
